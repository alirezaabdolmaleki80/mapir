package com.alireza.japa;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ApiService {
    private RequestQueue requestQueue;
    public ApiService(Context context){
        this.requestQueue= Volley.newRequestQueue(context);
    }

    public void sendRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener){
        StringRequest stringRequest=new StringRequest(url,listener,errorListener);
        requestQueue.add(stringRequest);
    }
}
