package com.alireza.japa;

import com.mapbox.geojson.LineString;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.LineManager;
import com.mapbox.mapboxsdk.plugins.annotation.LineOptions;

import org.json.JSONObject;

import ir.map.sdk_map.Mapir;
import ir.map.sdk_map.MapirStyle;
import ir.map.sdk_map.maps.MapView;
import okhttp3.ResponseBody;

public class RouteService {
private MapView mapView;
private MapboxMap map;
private Style mapStyle;
    String getGeometryObject(ResponseBody responseBody) {
        try {
            JSONObject bodyJson = new JSONObject(responseBody.string());
            return bodyJson.getJSONArray("routes").getJSONObject(0).get("geometry").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    void showRouteOnMap(String geometry) {
        LineManager lineManager = new LineManager(mapView, map, mapStyle, "hw-secondary-tertiary");
        LineString routeLine = LineString.fromPolyline(geometry, 5); // second parameter must be 5
        LineOptions lineOptions = new LineOptions()
                .withGeometry(routeLine)
                .withLineColor("#ff5252")
                .withLineWidth(5f);
        lineManager.create(lineOptions);
    }
}
