package com.alireza.japa;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Splashsceen extends Activity {
    private MediaPlayer sound;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_sceen);
        sound = MediaPlayer.create(this, R.raw.music);
        sound.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(Splashsceen.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        },3000);

    }

}
